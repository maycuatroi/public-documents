# Clean Code For Python

[![](https://img.shields.io/badge/python-3.7+-blue.svg)](https://www.python.org/download/releases/3.8.3/)

## Mục lục
  1. [Giới thiệu](#giới-thiệu)
  2. [Biến](#biến)
  3. [Functions](#functions)
  4. [Objects and Data Structures](#objects-and-data-structures)
  5. [Classes](#classes)
     1. [S: Đơn nhiệm - Single Responsibility Principle (SRP)](#single-responsibility-principle-srp)
     2. [O: Đóng và Mở - Open/Closed Principle (OCP)](#openclosed-principle-ocp)
     3. [L: Tính khả dĩ thay thế - Liskov Substitution Principle (LSP)](#liskov-substitution-principle-lsp)
     4. [I: Chia nhỏ interface - Interface Segregation Principle (ISP)](#interface-segregation-principle-isp)
     5. [D: Tính tương thích động - Dependency Inversion Principle (DIP)](#dependency-inversion-principle-dip)
  6. [Nguyên Lý DRY - Don"t repeat yourself (DRY)](#nguyên-lý-dry-dont-repeat-yourself-dry)

## Giới thiệu

Software engineering principles, from Robert C. Martin"s book
[*Clean Code*](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)  [*tiếng Việt*](https://gitlab.com/maycuatroi/public-documents/-/edit/master/Books/Clean%20Code/),
adapted cho Python. Mang tính thực tiễn và áp dụng cao.

Không phải mọi nguyên tắc ở đây đều phải được tuân thủ nghiêm ngặt, và thậm chí chỉ một số ít trong đây được đồng thuận và đặt làm quy tắc toàn cầu.
Đây là những hướng dẫn, chúng là những hướng dẫn được hệ thống hóa qua nhiều năm kinh nghiệm tập thể của các tác giả của *Clean Code*.

Lấy cảm hứng từ [clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript)

Bài viết gốc tiếng anh [clean-code-python](https://github.com/zedr/clean-code-python)


Target : Python3.7+

## Biến
### Sử dụng các tên biến có ý nghĩa và có thể phát âm được

**Bad:**
```python
import datetime


ymdstr = datetime.date.today().strftime("%y-%m-%d")
```

**Good**:
```python
import datetime


current_date: str = datetime.date.today().strftime("%y-%m-%d")
```
**[⬆ back to top](#mục-lục)**

### Sử dụng cùng một từ cho cùng một loại biến

**Bad:**
Ở đây, chúng tôi sử dụng ba tên khác nhau cho cùng một thực thể cơ bản: _user_
```python
def get_user_info(): 
    pass
def get_client_data(): 
    pass
def get_customer_record(): 
    pass
```

**Good**:
Nếu thực thể giống nhau, bạn nên đồng bộ nó trong các hàm chức năng của mình:
```python
def get_user_info(): 
    pass
def get_user_data(): 
    pass
def get_user_record(): 
    pass
```

**Even better**
Python (cũng) là một ngôn ngữ lập trình hướng đối tượng. Nếu thấy hợp lý, hãy đóng gói các chức năng cùng với bê các Class
  Entities trong code của bạn, chẳng hạn như instance attributes, property methods, hoặc các method:

```python
from typing import Union, Dict, Text


class Record:
    pass


class User:
    info : str

    @property
    def data(self) -> Dict[Text, Text]:
        return {}

    def get_record(self) -> Union[Record, None]:
        return Record()
        
```

**[⬆ back to top](#mục-lục)**

### Sử dụng tên có thể tìm kiếm

Chúng ta sẽ đọc nhiều hơn chúng ta viết. Điều quan trọng là mã chúng ta viết phải có thể đọc được và có thể tìm kiếm được. Bằng cách đặt tên *không chỉ* để trình thông dịch và bản thân có thể đọc hiểu, chúng ta đang làm mù mắt người đọc. Hãy đặt các cái tên có thể tìm kiếm được.

**Bad:**
```python
import time


# 86400 là cái gì ??:D??
time.sleep(86400)
```

**Good**:
```python
import time

# Khai báo chúng trong global namespace/Class.
SECONDS_IN_A_DAY = 60 * 60 * 24
time.sleep(SECONDS_IN_A_DAY)
```
**[⬆ back to top](#mục-lục)**

### Sử dụng các biến có tính giải thích
**Bad:**

Sử dụng trực tiếp index output của regex để gọi giá trị

```python
import re


address = "One Infinite Loop, Cupertino 95014"
city_zip_code_regex = r"^[^,\\]+[,\\\s]+(.+?)\s*(\d{5})?$"

matches = re.match(city_zip_code_regex, address)
if matches:
    print(f"{matches[1]}: {matches[2]}")
```

**Not bad**:

Tốt hơn, nhưng chúng ta vẫn phụ thuộc nhiều vào việc đọc output của regex.

```python
import re


address = "One Infinite Loop, Cupertino 95014"
city_zip_code_regex = r"^[^,\\]+[,\\\s]+(.+?)\s*(\d{5})?$"
matches = re.match(city_zip_code_regex, address)

if matches:
    city, zip_code = matches.groups()
    print(f"{city}: {zip_code}")
```

**Good**:

Giảm sự phụ thuộc vào regex bằng cách đặt tên cho các phần(patterns) bên trong.

```python
import re


address = "One Infinite Loop, Cupertino 95014"
city_zip_code_regex = r"^[^,\\]+[,\\\s]+(?P<city>.+?)\s*(?P<zip_code>\d{5})?$"

matches = re.match(city_zip_code_regex, address)
if matches:
    print(f"{matches['city']}, {matches['zip_code']}")
```
**[⬆ back to top](#mục-lục)**

### Đừng xoắn não - Avoid Mental Mapping

Đừng ép người đọc code compile bằng não để dịch ý nghĩa của biến. Rõ ràng là tốt hơn ngầu.

**Bad:**
```python
seq = ("Austin", "New York", "San Francisco")

for item in seq:
    #do_stuff()
    #do_some_other_stuff()

    # Khoan, `item` là cái gì?
    print(item)
```

**Good**:
```python
locations = ("Austin", "New York", "San Francisco")

for location in locations:
    #do_stuff()
    #do_some_other_stuff()
    # ...
    print(location)
```
**[⬆ back to top](#mục-lục)**


### Đừng thêm context không cần thiết

Nếu tên Class/Object của bạn cho bạn biết điều gì đó rồi, đừng lặp lại điều đó trong tên biến của bạn.

**Bad:**

```python
class Car:
    car_name: str
    car_make: str
    car_model: str
    car_color: str
```

**Good**:

```python
class Car:
    name: str
    make: str
    model: str
    color: str
```

**[⬆ back to top](#mục-lục)**

### Sử dụng các đối số mặc định thay vì dấu tròn hoặc điều kiện

**Lươn lẹo - Tricky**

Tại sao ?? :D ??

```python
import hashlib


def create_micro_brewery(name):
    name = "Hipster Brew Co." if name is None else name
    slug = hashlib.sha1(name.encode()).hexdigest()
    # etc.
```

... khi bạn có thể chỉ định một default argument (tham số) thay thế? Điều này cũng làm rõ rằng bạn đang mong đợi một String làm tham số.

**Good**:

```python
from typing import Text
import hashlib


def create_micro_brewery(name: Text = "Hipster Brew Co."):
    slug = hashlib.sha1(name.encode()).hexdigest()
    # etc.
```

**[⬆ back to top](#mục-lục)**
## **Functions**
### Tham số truyền vào (nhỏ hơn hoặc bằng 2 là lý tưởng)
Việc giới hạn số lượng tham số của Function là vô cùng quan trọng vì nó giúp kiểm tra Function của bạn dễ dàng hơn. Có nhiều hơn ba dẫn đến bùng nổ tổ hợp, nơi bạn phải kiểm tra hàng tấn trường hợp khác nhau với mỗi bộ tham số khác nhau.

Lý tưởng nhất là không cần quan tâm vào tham số truyền vào. Một hoặc hai tham số là ok, và ba tham số nên tránh. Bất cứ điều gì nhiều hơn thế nên được xem lại. Thông thường, nếu bạn có nhiều hơn hai tham số thì hàm của bạn đang ôm đồm thực hiện quá nhiều. Trong trường hợp cần nhiều thông tin truyền vào, một đối tượng sẽ được dùng làm tham số.

**Bad:**
```python
def create_menu(title, body, button_text, cancellable):
    pass
```

**Phong cách Java**:
```python
class Menu:
    def __init__(self, config: dict):
        self.title = config["title"]
        self.body = config["body"]
        # ...

menu = Menu(
    {
        "title": "My Menu",
        "body": "Something about my menu",
        "button_text": "OK",
        "cancellable": False
    }
)
```

**Also good**
```python
from typing import Text


class MenuConfig:
    """A configuration for the Menu.

    Attributes:
        title: The title of the Menu.
        body: The body of the Menu.
        button_text: The text for the button label.
        cancellable: Can it be cancelled?
    """
    title: Text
    body: Text
    button_text: Text
    cancellable: bool = False


def create_menu(config: MenuConfig) -> None:
    title = config.title
    body = config.body
    # ...


config = MenuConfig()
config.title = "My delicious menu"
config.body = "A description of the various items on the menu"
config.button_text = "Order now!"
# The instance attribute overrides the default class attribute.
config.cancellable = True

create_menu(config)
```

**Fancy**
```python
from typing import NamedTuple


class MenuConfig(NamedTuple):
    """A configuration for the Menu.

    Attributes:
        title: The title of the Menu.
        body: The body of the Menu.
        button_text: The text for the button label.
        cancellable: Can it be cancelled?
    """
    title: str
    body: str
    button_text: str
    cancellable: bool = False


def create_menu(config: MenuConfig):
    title, body, button_text, cancellable = config
    # ...


create_menu(
    MenuConfig(
        title="My delicious menu",
        body="A description of the various items on the menu",
        button_text="Order now!"
    )
)
```

**Even fancier**
```python
from typing import Text
from dataclasses import astuple, dataclass


@dataclass
class MenuConfig:
    """A configuration for the Menu.

    Attributes:
        title: The title of the Menu.
        body: The body of the Menu.
        button_text: The text for the button label.
        cancellable: Can it be cancelled?
    """
    title: Text
    body: Text
    button_text: Text
    cancellable: bool = False

def create_menu(config: MenuConfig):
    title, body, button_text, cancellable = astuple(config)
    # ...


create_menu(
    MenuConfig(
        title="My delicious menu",
        body="A description of the various items on the menu",
        button_text="Order now!"
    )
)
```

**Even fancier, Python3.8+ only**
```python
from typing import TypedDict, Text


class MenuConfig(TypedDict):
    """A configuration for the Menu.

    Attributes:
        title: The title of the Menu.
        body: The body of the Menu.
        button_text: The text for the button label.
        cancellable: Can it be cancelled?
    """
    title: Text
    body: Text
    button_text: Text
    cancellable: bool


def create_menu(config: MenuConfig):
    title = config["title"]
    # ...


create_menu(
    # You need to supply all the parameters
    MenuConfig(
        title="My delicious menu", 
        body="A description of the various items on the menu",
        button_text="Order now!",
        cancellable=True
    )
)
```
**[⬆ back to top](#mục-lục)**

### Các hàm chỉ nên làm một việc

Đây là quy tắc quan trọng nhất trong software engineering. 
Khi các function thực hiện nhiều hơn một việc, chúng sẽ đóng gói, kiểm tra và suy luận hơn.
Khi bạn có thể tách một function chỉ thực hiện một hành động, chúng có thể được tái cấu trúc lại dễ dàng và code của bạn sẽ rõ ràng hơn nhiều. 
Bạn chỉ cần thực hiện tốt duy nhất điều này bạn cũng đã là 1 Devs hàng đầu trong ngành rồi.

**Bad:**
```python
from typing import List


class Client:
    active: bool


def email(client: Client) -> None:
    pass


def email_clients(clients: List[Client]) -> None:
    """Filter active clients and send them an email.
    """
    for client in clients:
        if client.active:
            email(client)
```

**Good**:
```python
from typing import List


class Client:
    active: bool


def email(client: Client) -> None:
    pass


def get_active_clients(clients: List[Client]) -> List[Client]:
    """Filter active clients.
    """
    return [client for client in clients if client.active]


def email_clients(clients: List[Client]) -> None:
    """Send an email to a given list of clients.
    """
    for client in get_active_clients(clients):
        email(client)
```

Bạn có thấy dụng generators tại đây hợp lý hơn không?

**Even better**
```python
from typing import Generator, Iterator


class Client:
    active: bool


def email(client: Client):
    pass


def active_clients(clients: Iterator[Client]) -> Generator[Client, None, None]:
    """Only active clients"""
    return (client for client in clients if client.active)


def email_client(clients: Iterator[Client]) -> None:
    """Send an email to a given list of clients.
    """
    for client in active_clients(clients):
        email(client)
```


**[⬆ back to top](#mục-lục)**

### Tên hàm phải nói lên những gì chúng sẽ làm

**Bad:**

```python
class Email:
    def handle(self) -> None:
        pass

message = Email()
# handle là cái quái gì?
message.handle()
```

**Good:**

```python
class Email:
    def send(self) -> None:
        """Send this message"""

message = Email()
message.send()
```

**[⬆ back to top](#mục-lục)**

### Các hàm chỉ nên có một cấp độ trừu tượng (level of abstraction)

Khi bạn có nhiều hơn một cấp độ trừu tượng, function của bạn đang ôm đồm quá nhiều. 
Việc chia nhỏ các function dẫn đến khả năng tái sử dụng và test dễ dàng hơn.

**Bad:**

```python
# type: ignore

def parse_better_js_alternative(code: str) -> None:
    regexes = [
        # ...
    ]

    statements = code.split('\n')
    tokens = []
    for regex in regexes:
        for statement in statements:
            pass

    ast = []
    for token in tokens:
        pass

    for node in ast:
        pass
```

**Good:**

```python
from typing import Tuple, List, Text, Dict


REGEXES: Tuple = (
   # ...
)


def parse_better_js_alternative(code: Text) -> None:
    tokens: List = tokenize(code)
    syntax_tree: List = parse(tokens)

    for node in syntax_tree:
        pass


def tokenize(code: Text) -> List:
    statements = code.split()
    tokens: List[Dict] = []
    for regex in REGEXES:
        for statement in statements:
            pass

    return tokens


def parse(tokens: List) -> List:
    syntax_tree: List[Dict] = []
    for token in tokens:
        pass

    return syntax_tree
```

**[⬆ back to top](#mục-lục)**

### Không sử dụng các flags làm function parameters

Cờ cho người dùng của bạn biết rằng function này thực hiện nhiều hơn một việc.
**Các hàm nên làm một việc.** 
Tách hàm nếu chúng phân tách dựa trên 1 boolean parameter.

**Bad:**

```python
from typing import Text
from tempfile import gettempdir
from pathlib import Path


def create_file(name: Text, temp: bool) -> None:
    if temp:
        (Path(gettempdir()) / name).touch()
    else:
        Path(name).touch()
```

**Good:**

```python
from typing import Text
from tempfile import gettempdir
from pathlib import Path


def create_file(name: Text) -> None:
    Path(name).touch()


def create_temp_file(name: Text) -> None:
    (Path(gettempdir()) / name).touch()
```

**[⬆ back to top](#mục-lục)**

### Tránh tác động phụ (side effects)

Một hàm tạo ra side effects nếu nó thực hiện bất kỳ điều gì khác ngoài việc nhận vào một giá trị và trả về một hoặc các giá trị. 

Ví dụ: một side effects có thể là ghi file cache, sửa đổi một số biến toàn cục hoặc vô tình nhắn tin đòi đốt công ty.

Giả sử, bây giờ có các side effects trong trương code của bạn - ví dụ, như trong ví dụ phía trên, ghi file ra cache.

Trong những trường hợp này, bạn cần quy hoạch và chỉ ra nơi đang sinh ra các side effects đó. Không dùng một đống function/method và class để ghi vào một cùng 1 file. Thay vào đó, hãy có một (và chỉ một) component thực hiện điều đó.

Main point là để tránh những lỗi phổ biến như chia sẻ trạng thái giữa các objects mà không tuân theo structure nào, sử dụng các kiểu dữ liệu có thể thay đổi có thể được ghi vào bất kỳ thứ gì hoặc sử dụng một instance của 1 class và không quy hoạch vào nơi các side effects xảy ra. Nếu bạn làm được điều này, bạn sẽ hạnh phúc hơn đại đa số các lập trình viên khác.

**Bad:**

```python
# type: ignore

# This is a module-level name.
# It"s good practice to define these as immutable values, such as a string.
# Tuy nhiên...
fullname = "Ryan McDermott"

def split_into_first_and_last_name() -> None:
    # The use of the global keyword here is changing the meaning of the
    # the following line. This function is now mutating the module-level
    # state and introducing a side-effect!
    global fullname
    fullname = fullname.split()

split_into_first_and_last_name()

# MyPy will spot the problem, complaining about 'Incompatible types in 
# assignment: (expression has type "List[str]", variable has type "str")'
print(fullname)  # ["Ryan", "McDermott"]

# OK. It worked the first time, but what will happen if we call the
# function again?
```

**Good:**
```python
from typing import List, AnyStr


def split_into_first_and_last_name(name: AnyStr) -> List[AnyStr]:
    return name.split()

fullname = "Ryan McDermott"
name, surname = split_into_first_and_last_name(fullname)

print(name, surname)  # => Ryan McDermott
```

**Also good**
```python
from typing import Text
from dataclasses import dataclass


@dataclass
class Person:
    name: Text

    @property
    def name_as_first_and_last(self) -> list:
        return self.name.split() 


# The reason why we create instances of classes is to manage state!
person = Person("Ryan McDermott")
print(person.name)  # => "Ryan McDermott"
print(person.name_as_first_and_last)  # => ["Ryan", "McDermott"]
```

**[⬆ back to top](#mục-lục)**

## **Objects and Data Structures**

*Coming soon*

**[⬆ back to top](#mục-lục)**

## **Classes**

### **Single Responsibility Principle (SRP)**
### **Open/Closed Principle (OCP)**
### **Liskov Substitution Principle (LSP)**
### **Interface Segregation Principle (ISP)**
### **Dependency Inversion Principle (DIP)**

*Coming soon*

**[⬆ back to top](#mục-lục)**

## Nguyên Lý DRY - Don"t repeat yourself (DRY)

Don't Repeat Yourself hay DRY là nguyên lý cơ bản nhất: hạn chế tối thiểu việc viết các đoạn code lặp đi lặp lại nhiều lần chỉ để thực hiện cùng một công việc.

Chúng tôi muốn tính chỉ số BMI của năm người. 

Đây là cách chúng ta có thể làm điều đó bằng Python:

**Bad**

```python
# Person data = [weight_kg, height_m]
person_Bob = [80, 1.62]
person_Jane = [69, 1.53]
person_Goby = [80, 1.66]
person_Sel = [80, 1.79]
person_John = [72, 1.60]

bmi_person1 = int(person_Bob[0] / person_Bob[1] ** 2)
print("bmi {} = {}".format('Bob', bmi_person1))

bmi_person2 = int(person_Jane[0] / person_Jane[1] ** 2)
print("bmi {} = {}".format('Jane', bmi_person2))

bmi_person3 = int(person_Goby[0] / person_Goby[1] ** 2)
print("bmi {} = {}".format('Goby', bmi_person3))

bmi_person4 = int(person_Sel[0] / person_Sel[1] ** 2)
print("bmi {} = {}".format('Sel', bmi_person4))

bmi_person5 = int(person_John[0] / person_John[1] ** 2)
print("bmi {} = {}".format('John', bmi_person5))

```

**Good**

```python
class Person:
    def __init__(self, name: str, weight: float, height: float):
        self.name = name
        self.weight = weight
        self.height = height

    @property
    def bmi(self) -> int:
        """
        Calculate BMI from weight in kg and height in meters
        :return: bmi
        """
        bmi = int(self.weight / self.height ** 2)
        return bmi


# person_raw_data = [name, weight_kg, height_m]
person_raw_data = [['Bob', 80, 1.62],
                   ['Jane', 69, 1.53],
                   ['Goby', 80, 1.66],
                   ['Sel', 80, 1.79],
                   ['John', 72, 1.60]]

persons = []
for name, weight, height in person_raw_data:
    person = Person(name=name, weight=weight, height=height)
    persons.append(person)

for person in persons:
    print("bmi {} = {}".format(person.name, person.bmi))

```

**[⬆ back to top](#mục-lục)**
