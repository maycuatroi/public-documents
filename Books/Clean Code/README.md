# Mục lục


Chương 1: [Code thơm](Chương 1 - code thơm.md)

Chương 2: [Đặt tên biến](Chương 2 - Đặt tên biến.md)

Chương 3: Functions 31

Chương 4: Comments 53

Chương 5: Formatting 75

Chương 6: Objects and Data Structures 93

Chương 7: Error Handling 103

Chương 8: Boundaries 113

Chương 9: Unit Tests 121

Chương 10: Classes 135

Chương 11: Systems 153

Chương 12: Emergence 171

Chương 13: Concurrency 177

Chương 14: Successive Refinement 193

Chương 15: JUnit Internals 251

Chương 16: Refactoring SerialDate 267

Chương 17: Smells and Heuristics 285

---

Sách gốc : [*Clean Code*](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)

Các bản dịch có sử dụng :

[NQT-K4DNC](https://github.com/quoctinnguyen8/Clean-Code---Tieng-Viet)