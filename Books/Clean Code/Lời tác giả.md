# Code thơm

Robert C. Martin Series

Mục tiêu của Series này là cải thiện chất lượng code của các software craftmanship (nghệ nhân lập trình). Những cuốn sách trong Series này là kỹ nghệ quan trọng và thực tiễn. Các tác giả là những người thợ và chuyên gia có kinh nghiệm cao, chuyên viết về những gì đang thực sự hoạt động trong thực tế thay vì những gì có thể hoạt động trên lý thuyết. Bạn sẽ đọc về những gì tác giả đã làm, không phải những gì ông ấy nghĩ bạn nên làm. Nếu cuốn sách về lập trình, sẽ có rất nhiều code. Nếu cuốn sách nói về quản lý, sẽ có rất nhiều trường hợp điển hình từ các dự án thực tế. Đây là những cuốn sách mà tất cả những Dev yêu nghề sẽ luôn có trên giá sách của họ. Cuốn sách này sẽ được ghi nhớ vì nó khác biệt và hướng dẫn các chuyên gia trở thành một nghệ nhân thực thụ.

_- Thực sự Sofware engineering nên được hiểu là Kỹ nghệ phần mềm, nó mang nhiều kỹ thuật và yêu cầu sự rèn luyện. Thay vì chỉ là gõ code_

### Tủ sách:
_Managing Agile Projects_

Sanjiv Augustine

_Agile Estimating and Planning_

Mike Cohn

_Working Effectively with Legacy Code_

Michael C. Feathers

_Agile Java™: Crafting Code with Test-Driven Development_

Jeff Langr

_Agile Principles, Patterns, and Practices in C#_

Robert C. Martin and Micah Martin

_Agile Software Development: Principles, Patterns, and Practices_

Robert C. Martin

_Clean Code: A Handbook of Agile Software Craftsmanship_

Robert C. Martin

_UML For Java™ Programmers_

Robert C. Martin

_Fit for Developing Software: Framework for Integrated Tests_

Rick Mugridge and Ward Cunningham

_Agile Software Development with SCRUM_

Ken Schwaber and Mike Beedle

_Extreme Software Engineering: A Hands on Approach_

Daniel H. Steinberg and Daniel W. Palmer

