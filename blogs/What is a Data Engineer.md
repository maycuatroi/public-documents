# What is a Data Engineer?

Data Engineer doesn't always come with the same sexy connotations as something like Data Scientist. However, looks aren't everything, and the work that a Data Engineer does make up a significant portion of the work carried out by a Data Scientist.

Understandably, topics like Machine Learning and AI will always win the popularity contest, especially as they grow in popularity in mainstream media. However, a good chunk of the work that sits behind these concepts stems from Data Engineering work.

Now, this isn't an article about the battle of Data Engineers vs. Data Scientists. There's no beef here. Instead, this article comes off the back of the sea of items I've seen recently talking about this exact point: 80% of a Data Scientists work is data preparation and cleansing.

So I'm going to talk about why I feel Data Engineering is a vital field based on what it enables and how together, with Data Science, provides the industry's latest tech backbone.

## AI and Machine Learning models require data.

Talk to any Data Scientist, and they'll tell you that obtaining data, especially a source that has absolutely everything they require for their model, is a distant dream.

In the real world, data sets that are this helpful are rare, and that's where the first skill of the Data Engineer comes into play. We dedicate a lot of our time to pulling data sets from various sources, most of the time, into a central hub to be utilized together.

Now anyone can go and download a static data set from a website. The benefit of a Data Engineer is just that: engineering. Not only can they provide you a multitude of data from disparate sources, but they can also do it in a way that repeatable, frequently updated, and, if required, even in real-time.

## Clean Data lead to Better Model.

All the data required to build your AI or ML model is now coming in frequently to your central data hub.

The next step required before getting your AI and ML model off the ground is preparing the data. Now, this is where this article stemmed from for me. The sheer amount of items I've seen about this and even a Forbes survey in 2016 is just insane.

![image](https://thumbor.forbes.com/thumbor/960x0/https%3A%2F%2Fblogs-images.forbes.com%2Fgilpress%2Ffiles%2F2016%2F03%2FTime-1200x511.jpg)
> source : [Cleaning Big Data: Most Time-Consuming, Least Enjoyable Data Science Task, Survey Says](https://www.forbes.com/sites/gilpress/2016/03/23/data-preparation-most-time-consuming-least-enjoyable-data-science-task-survey-says/#4b08e99d6f63)

The Forbes survey states that 80% of Data Science work is data prep and that 75% of Data Scientists find this to be the most tedious aspect of the job.

👋 Guess what? This is where the Data Engineer also thrives. We spend so much time manipulating data that a lot of this work comes second nature. Whether it's joining data sets together, cleaning out null and erroneous values, driving strings into features, or aggregating data, we've got you covered.

As a freebie, again, all of this will be built in a repeatable way, so as data is updated, it also cleansed, providing a consistent source of fresh, clean data.

And as a two, you also get the added benefit of freeing up your Data Scientists to squeeze every last inch out of the model and keep their morale higher as the most tedious part of their job has now disappeared.

## Thank god: Finally, we build a model

We're finally there. After all, this upfront effort work on the model can finally begin, and you might think this is where the Data Engineer disappears into the abyss.

However, anyone who's built AI and ML models will know that life isn't that simple. As the model is being constructed, multiple iterations of the above will be cycled through as more questions are asked and extra data required for answers.

This is where the Data Scientist shines through, and I can't stress enough how important it is to let them do their thing here. Everything I've talked about so far isn't me trying to say that Data Engineers are better or worth more. It shows how they can enable more efficient workflows for Data Scientists to get down to the nitty-gritty.

## Return to reality: A model is only useful if someones going to use it

Work is complete on the first iteration of the model. We can all pack up and go home, right? As most of you already know, this isn't the case. The model might be built, but few things are left to consider: how will it be used in the real world and how quickly will it become stale.
The AI or ML model's purpose was to solve a real-world problem, so it now needs to be applied. Usually, this means being implemented into an application, or maybe it's used for segmentation or predictive marketing.

A Data Engineer will add the model to a data pipeline that processes your whole user base against the model and segments them accordingly. They can then use this to trigger automated comms or publish the pieces to a data layer to enable part targeted content on your website.
And if you're worried that the model will become stale in a few months, don't fear. A sound Data Engineer will work with the Data Scientists and translate their work into something that can be continuously updated—feeding in new data, rebuilding the model, and automatically publishing it.

## So what am I getting at?

Whether you're considering hiring a Data Engineer or are looking at getting into the field of data but don't know where to start, it's clear to see that Data Engineering is an important field to consider.

You can see just how much of the work behind building a data model and making it live can be accomplished by a Data Engineer.

The efficiencies gained mean that getting to the point of building the model will be quicker, and the models will undoubtedly be better, as the Data Scientists have more time to spend tweaking and improving them.